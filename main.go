package main

import (
	"context"

	"github.com/sirupsen/logrus"
)

// contextual logging setup

var logKey = struct{}{}

func logContext(ctx context.Context, log *logrus.Entry) context.Context {
	return context.WithValue(ctx, logKey, log)
}

func logContextAddFields(ctx context.Context, fields logrus.Fields) context.Context {
	log := logger(ctx)
	return context.WithValue(ctx, logKey, log.WithFields(fields))
}

func logger(ctx context.Context) *logrus.Entry {
	log, ok := ctx.Value(logKey).(*logrus.Entry)
	if !ok {
		return logrus.WithFields(logrus.Fields{})
	}
	return log
}

// -----------------------------------------------------------------------------

func main() {
	l := logrus.WithFields(logrus.Fields{"number": 1, "size": 10})
	ctx := logContext(context.Background(), l)

	request(ctx, "test")
}

func request(ctx context.Context, s string) {
	log := logger(ctx)

	log.Println(s)
	c := logContextAddFields(ctx, logrus.Fields{"process": 54321})
	process(c, "going deeper")
}

func process(ctx context.Context, s string) {
	log := logger(ctx)

	log.Println("subprocess", s)
}
